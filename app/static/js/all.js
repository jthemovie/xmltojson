$.extend({
    keys: function (obj) {
        var a = [];
        $.each(obj, function (k) {
            a.push(k)
        });
        return a;
    },

    firstOrNull: function (col, attr, value) {
        var items = $.grep(col, function (obj) {
            return obj[attr] == value;
        });
        if (items.length == 1) {
            return items[0]
        }
        return null;
    },

    count: function (col, fn) {
        var count = 0;
        $.each(col, function (idx, item) {
            if (fn(item)) {
                count = count + 1;
            }
        });
        return count;
    },
});

(function ($, JsDiff, undefined) {
    'use strict';

    function diffIt (text_A, text_B, options) {
        if (!exists(text_A) || !exists(text_B)) {
            return '';
        }
        var diff = JsDiff[options.diffMethod](text_A, text_B);
        return diff.map(function(part){
            var span = $('<span>').text(part.value),
                cssClass;
            if (part.added) {
                cssClass = options.addedClass;
            } else if (part.removed) {
                cssClass = options.removedClass;
            }
            span.addClass(cssClass);
            return span;
        });
    };

    $.fn.diffIt = function (text_A, text_B, options) {
        options = $.extend({}, $.fn.diffIt.config, options);
        return $(this).html(diffIt(text_A, text_B, options));
    };

    $.fn.diffIt.config = {
        addedClass: 'diffIt-added',
        removedClass: 'diffIt-removed',
        diffMethod: 'diffWords'
    };

}(jQuery, JsDiff));

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function exists(val) {
    return typeof val !== 'undefined' && val != null;
}

function randomString(length) {
    return Math.round(
        (Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))
    ).toString(36).slice(1);
}

function capitalize(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

// functions
Ractive.prototype.remove = function (keypath) {
    var lastDot = keypath.lastIndexOf('.');
    var array = keypath.substr(0, lastDot);
    var index = +( keypath.substring(lastDot + 1) );

    return this.splice(array, index, 1)
};

// decorators
var selectizedDecorator = function (node) {
    $(node).selectize();
    return {
        teardown: function () {
        }
    };
};

var sliderDecorator = function (node) {
    $(node).slider({
        tooltip: 'always',
    });
    return {
        teardown: function () {
        }
    };
};

var tooltipDecorator = function (node) {
    $(node).tooltip();
    return {
        teardown: function () {
        }
    };
};

var slimscrollDecorator = function (node) {
    $(node).slimScroll({
        height: 550,

    });
    return {
        teardown: function () {
        }
    };
};

var diffItDecorator = function (node, text_A, text_B) {
    $(node).diffIt(text_A, text_B);
    return {
        teardown: function(){}
    };
};

var R = Ractive.extend({
    decorators: {
        selectize: selectizedDecorator,
        slider: sliderDecorator,
        tooltip: tooltipDecorator,
        slimscroll: slimscrollDecorator,
        diffIt: diffItDecorator,
    }
});


var API = function (endpoint) {
    this.endpoint = endpoint;
};

$.extend(API.prototype, {

    get: function (success) {
        return $.ajax({
            url: this.endpoint,
            type: 'GET',
            success: success,
        });
    },

    get_by_id: function (id, success) {
        return $.ajax({
            url: this.endpoint + '/' + id,
            type: 'GET',
            success: success,
        });
    },

    remove: function (object, success) {
        return $.ajax({
            url: this.endpoint + '/' + object._id,
            type: 'DELETE',
            success: success,
        });
    },

    save: function (object, success) {
        var endpoint = this.endpoint;
        if (object._id) {
            endpoint = this.endpoint + '/' + object._id;
        }

        return $.ajax({
            url: endpoint,
            type: 'POST',
            data: JSON.stringify(object),
            success: success,
        });
    }

});

var ProjectsApi = function() {
    API.call(this, '/api/projects');
};

$.extend(ProjectsApi.prototype, API.prototype, {

    get_defaults_by_id: function (project_id, success) {
        return $.ajax({
            url: this.endpoint + '/' + project_id + '/defaults',
            type: 'GET',
            success: success,
        });
    },

    save: function (project, success, partial) {
        var partial = exists(partial) ? true : false;

        var endpoint = this.endpoint;
        if (project._id) {
            endpoint = this.endpoint + '/' + project._id;
        }

        if (partial) {
            delete project['segments']
        }

        return $.ajax({
            url: endpoint,
            type: 'POST',
            data: JSON.stringify(project),
            success: success,
        });
    },

    submit: function (project, success) {
        var payload = { '_id': project._id };
        if (exists(project.general_reviewers_evaluation)) {
            payload['comment'] = project.general_reviewers_evaluation;
        }
        return $.ajax({
            url: this.endpoint + '/' + project._id + '/submit',
            type: 'POST',
            success: success,
            data: JSON.stringify(payload),
        });

    },
    upload: function (project_id, files) {
        var url = this.endpoint + '/'+ project_id +'/upload';

        var data = new FormData();
        $(files).each(function() {
            data.append(this.name, this);
        });

        return $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        });
    }
});

var UsersApi = function() {
    API.call(this, '/api/users');
};

$.extend(UsersApi.prototype, API.prototype, {

    get_me: function (success) {
        return $.ajax({
            url: this.endpoint + '/me',
            type: 'GET',
            success: success,
        })
    }

});


var ContentWrapper = R.extend({
    el: '.content-wrapper',
});

var CreateOrUpdateView = function (context, params) {
    this.context = context;
    this.params = params;

    this.resource = {};
    this.edition = exists(context.params.id);

    this.title = params.resource_name + ' New';
    if (this.edition) {
        this.title = params.resource_name + ' Edit';
    }
};


$.extend(CreateOrUpdateView.prototype, {

    fetch_organization: function (self) {
        return $.ajax({
            url: '/api/organizations/' + store.get('organization_id'),
            method: 'GET',
            success: function (data) {
                self.organization = data.message;
            }
        });
    },

    resolved_deferred: function () {
        var d = $.Deferred();
        d.resolve();
        return d;
    },

    fetch_resource: function () {
        return $.Deferred();
    },

    edition_enabled: function () {
        return false;
    },

    save: function (resource) {

    },

    run: function () {
        var d1 = this.fetch_organization(this);
        var d2 = this.fetch_resource(this);

        var self = this;

        $.when(
            d1, d2
        ).then(function () {

            var data = {
                title: self.title,
                subtitle: '',
                resource: self.resource,
                organization: self.organization,
                context: store.get('role'),
                edition: self.edition,
                edition_enabled: self.edition_enabled(),
                partial: self.params.template
            };

            var tmpl = ContentWrapper({
                template: '#content-wrapper-resource-new-template',
                data: data,
            });

            tmpl.on('toggleAdvancedSettings', function (event) {
                var basicSettingsText = "Basic settings";
                var advancedSettingsText = "Advanced settings";

                var btnText = event.node.innerText == advancedSettingsText ? basicSettingsText : advancedSettingsText;
                $(event.node).text(btnText);
            });

            tmpl.on('save', function (event) {
                if (!isValid("#form")) {
                    return;
                }

                self.save(tmpl.get('resource'), self.edition);
            });
        });

    }
});

var ListView = function (params) {
    this.params = params;
    this.resources = {};
};

$.extend(ListView.prototype, {

    fetch_resources: function (self) {
        return self.params.api.get(function (data) {
            self.resources = data.message;
        });
    },

    init: function (tmpl) {

    },

    run: function () {

        var self = this;

        $.when(
            self.fetch_resources(self)
        ).then(function () {

            var internal_data = {
                context: store.get('role'),
                resources: self.resources,
                show_all: false,

                format: function (time) {
                    return moment(time).fromNow();
                }
            };

            var data = $.extend(internal_data, self.params.data);

            var tmpl = ContentWrapper({
                template: '#content-wrapper-resource-list-template',
                data: data,
                computed: self.params.computed,
            });

            tmpl.on('show-all', function (event) {
                event.original.preventDefault();

                this.set('show_all', !this.get('show_all'));

                var txt = event.node.innerText == "Show All" ? "Hide" : "Show All";
                $(event.node).text(txt);
            });

            tmpl.on('delete', function (event) {
                event.original.preventDefault();

                var resource = this.get(event.keypath);
                show_confirmation_and_execute(
                    self.params.resource_name + " deletion is final and can't be revoked!",
                    self.params.resource_name + " has beed deleted!",
                    function () {
                        return self.params.api.remove(resource, function (data) {
                            tmpl.remove(event.keypath);
                        });
                    }
                );
            });

            self.init(tmpl);

        });

    }
});


swal.setDefaults({
    allowEscapeKey: true,
    confirmButtonColor: "#23b7e5",
    closeOnConfirm: false,
    showLoaderOnConfirm: true
});

function isValid(locator) {
    var form = $(locator);
    var parsleyForm = form.parsley({
        excluded: 'input[type=button], input[type=submit], input[type=reset], :hidden, :disabled',
    });
    parsleyForm.validate();
    if (!parsleyForm.isValid()) {
        return false;
    }
    return true
}

function show_success_modal (text) {
    swal("Good job!", text, "success");
};

function show_fail_modal (text) {
    swal("Something went wrong!", text, "warning");
};

function show_confirmation_and_execute(text, confirmation_text, fn) {
    swal({
            title: "Please confirm",
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, I am aware!",
        }, function() {
            fn().done(function () {
                show_success_modal(confirmation_text);
            });
        }
    );
};

function show_textarea_modal_and_execute(title, fn) {
    swal({
        title: title,
        text: '<textarea id="sw_comment" placeholder="Write your comment here." class="form-control none-resizable" required minlength="1" rows="4" autofocus></textarea>',
        html: true,
        closeOnCancel: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: false,
        showCancelButton: true,
        }, function(isConfirm){
            if (isConfirm) {
                var field = $('#sw_comment');
                var inputValue = field.val();

                if (inputValue === false) {
                    return false;
                }
                if (inputValue === "") {
                    field.addClass('parsley-error');
                    return false;
                }

                fn(inputValue);
            }
        }
    );
};

$(document).ready(function () {
    setNavigation();
    setNavigationClickable();
    setUser();

    $(document).ajaxSuccess(function () {
        $('.alert-danger').addClass('hidden');
    });

});


// ajaxSetup needs to be outside of $(document).ready as it has to be executed before any $.ajax call
$.ajaxSetup({
    dataType: 'JSON',
    username: getCookie('token'),
    password: '',
    contentType: "application/json; charset=utf-8",
    statusCode: {
        401: function () {
            window.location.replace('/logout?token_expired=True');
        }
    },
    error: function (xhr, type, error) {
        var message = "<strong>Oh snap!</strong> Unexpected error occurred on server side. Please contact: marcin.dziedzic@pragmaticcoders.com"

        var error_msg = xhr.responseJSON;
        if (exists(error_msg) && error_msg.error == 'badrequest') {
            if (typeof error_msg.message === 'string') {
                message = "<strong>Oh snap!</strong> " + error_msg.message;
            } else {
                // get message of the first key and display it
                var keys = $.keys(error_msg.message);
                $.each(keys, function(idx, key) {
                    if (exists($('#' + key))) {
                        message = "<strong>Oh snap!</strong> " + error_msg.message[key];
                    }
                });
            }
        }

        var alert = $('.alert-danger');
        alert.html(message);
        alert.removeClass('hidden');
    },
});


function setNavigation() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);

    if (path === '') {
        path = '/projects';
    }

    $(".nav a").each(function () {
        var href = $(this).attr('href');
        if (path === href) {
            $(this).closest('li').addClass('active');
        }
    });
}

function setNavigationClickable() {
    $(".left-menu li").click(function (e) {
        $(".left-menu li").removeClass("active");
        $(this).addClass("active");
    });
}

var store = new Persist.Store('Accent Pharm');
function setUser() {
    new UsersApi().get_me(function (data) {
        store.set('user_id', data.message._id);
        store.set('username', data.message.username);
        store.set('role', data.message.role);
        store.set('organization_id', data.message.organization_id);
    });
}

// project wide functions
var PHASE_TO_LABEL = {
    'creation': 'Launch project',
    'review': 'Submit to translator',
    'commenting': 'Submit to manager',
    'dispute_resolution': 'Close project',
    'closed': 'Close project',
};

function submit_project(tmpl, keypath) {
    var project = tmpl.get(keypath);

    var _submit_project_fn = function() {
        return new ProjectsApi().submit(project, function (data) {
            tmpl.set(keypath + '.phase', data.message.phase);
        });
    }

    if (project.phase == 'review') {
        show_textarea_modal_and_execute("General reviewer's evaluation!", function (inputValue) {
            project.general_reviewers_evaluation = inputValue;
            _submit_project_fn().done(function () {
                show_success_modal("Project has beed submitted!");
            });
        });
    } else {
        show_confirmation_and_execute(
            "Project submission is final and can't be revoked!",
            "Project has beed submitted!",
            _submit_project_fn
        );
    }
};


// views

var CreateUpdateProjectView = function(context) {
    CreateOrUpdateView.call(this, context, {
        resource_name: 'Project',
        template: 'partial-project-form',
    });
};

$.extend(CreateUpdateProjectView.prototype, CreateOrUpdateView.prototype, {

    fetch_resource: function (self) {
        if (this.edition) {
            return new ProjectsApi().get_by_id(this.context.params.id, function (data) {
                self.resource = data.message;
            });
        } else {
            self.resource.expected_lqi = 95;
            return self.resolved_deferred();
        }
    },

    edition_enabled: function () {
        return store.get('role') == 'manager';
    },

    save: function (resource, edition) {
        // hack for selectize.js
        resource['client_name'] = $('#client_name').val();

        var translator_id = $('#translator').val();
        if (translator_id != '') {
            resource['translator_id'] = translator_id;
        }

        var reviewer_id = $('#reviewer').val();
        if (reviewer_id != '') {
            resource['reviewer_id'] = reviewer_id;
        }

        // hack for bootstrap-slider
        var expected_lqi = $('#expected-lqi').data('slider').getValue();
        resource['expected_lqi'] = expected_lqi;

        new ProjectsApi().save(resource, function (data) {
            page('/projects/' + data.message._id);
        }, edition);
    },

});

function projects() {

    var ProjectsListView = function() {
        ListView.call(this,
            {
                resource_name: 'Project',
                api: new ProjectsApi(),
                // ractivejs data and computed properties
                data: {
                    partial: 'partial-projects-list',
                    endpoint: '/projects',
                    display_show_all: true,
                    filter: function (item) {
                        var show_all = this.get('show_all');
                        if (show_all == false && item.phase == 'closed') {
                            return false;
                        }

                        var query = this.get('query');
                        if (exists(query)) {
                            return item.name.match(query)
                        }
                        return true;
                    },
                    submit_button_label: function(phase) {
                        return PHASE_TO_LABEL[phase];
                    },
                },
            }
        )
    };

    $.extend(ProjectsListView.prototype, ListView.prototype, {

        init: function (tmpl) {
            tmpl.on('submit', function (event) {
                event.original.preventDefault();
                submit_project(tmpl, event.keypath);
            });
        }

    });

    new ProjectsListView().run();
}

function project_create_or_update(context) {
    new CreateUpdateProjectView(context).run();
}

function project_details(context) {

    var project = {};
    var defaults = {};

    $.when(

        new ProjectsApi().get_by_id(context.params.id, function (data) {
            project = data.message;
        }),

        new ProjectsApi().get_defaults_by_id(context.params.id, function (data) {
            defaults = data.message;
        })

    ).then(function() {

        function get_comment(segment, role) {
            return $.firstOrNull(segment.comments, 'role', role);
        }

        function get_confirmation(segment, role) {
            return $.firstOrNull(segment.confirmations, 'role', role);
        }

        function create_segment(template) {
            var id = randomString(16);

            var source, translation, correction, filename;
            if (typeof template != 'undefined') {
                source = template.source;
                translation = template.translation;
                correction = template.correction;
                filename = template.filename;
            }

            return {
                id: id,
                source: source,
                translation: translation,
                correction: correction,
                filename: filename,
                reviewer_comment: null,
                checked: false,
                comments: [],
                confirmations: [],
            }
        }

        // enrich with data needed by frontend
        $.each(project.segments, function (idx, segment) {
            segment.checked = false;

            var value = get_comment(segment, 'reviewer');
            if (value) {
                segment['reviewer_comment'] = value;
            }

            var value = get_comment(segment, 'translator');
            if (value) {
                segment['translator_comment'] = value;
            }
        });

        // take first segment or null
        var active_segment = null;
        if (project.segments.length > 0) {
            active_segment = project.segments[0];
        }

        var projectDetailsTemplate = ContentWrapper({
            template: '#content-wrapper-project-details-template',
            data: {
                title: 'Projects Details',
                subtitle: '',
                project: project,
                active_segment: active_segment,

                // static data
                error_categories: defaults.error_categories,
                error_severities: defaults.error_severities,
                update_policy: defaults.update_policy,
                context: defaults.context,
                reviewer: defaults.reviewer,
                translator: defaults.translator,

                // utility functions
                capitalize: function (text) {
                    return text.charAt(0).toUpperCase() + text.slice(1);
                },
                confirmed: function (segment) {
                    if (project.phase == 'creation') {
                        return null;
                    } else if (project.phase == 'review') {
                        return get_confirmation(segment, 'reviewer');
                    } else {
                        var error_disagreement = exists(segment.error_disagreement);
                        if (!error_disagreement) {
                            return get_confirmation(segment, 'translator');
                        }
                        return false;
                    }
                },
                errors_count: function (error_severity) {
                    var segments = this.get('project.segments');
                    return $.count(segments, function (segment) {
                        return segment.error_severity == error_severity;
                    });
                },
            },
            computed: {
                disputed_segments_count: function() {
                    return 1;
                    var phase = this.get('project.phase');
                    if (phase == 'dispute_resolution' || phase == 'closed') {
                        var segments = this.get('project.segments');
                        return $.count(segments, function(segment) {
                            return exists(segment.error_disagreement);

                        });
                    }
                    return null;
                },
                submit_button_label: function() {
                    return PHASE_TO_LABEL[this.get('project.phase')]
                },
            },
            get_next_segment_index: function () {
                var project_segments = this.get('project.segments');
                var active_segment = this.get('active_segment');

                var idx = $.inArray(active_segment, project_segments);
                var next_idx = idx + 1;
                if (project_segments.length <= next_idx) {
                    next_idx = 0;
                }
                return next_idx;
            },
            confirm: function () {
                var active_segment = this.get('active_segment');
                var confirmation = get_confirmation(active_segment, this.get('context'));
                if (confirmation == null) {
                    confirmation = {
                        user_id: store.get('user_id'),
                        role: this.get('context'),
                        confirmed: true,
                    };
                    active_segment.confirmations.push(confirmation);
                }
            },
            push_comment: function (keypath, role) {
                var segment = this.get('active_segment');
                var comment = this.get(keypath);

                if (typeof comment !== 'undefined' && comment != null && comment.text != '') {
                    if (get_comment(segment, role) == null) {
                        var persistent_comment = {
                            user_id: store.get('user_id'),
                            role: role,
                            text: comment.text,
                        };
                        segment.comments.push(persistent_comment);
                        this.set(keypath, persistent_comment);
                    }
                }
            },
        });

        projectDetailsTemplate.on('create', function (event) {
            var new_segment = create_segment();

            this.push('project.segments', new_segment);
            this.set('active_segment', new_segment);

            $('#source').focus();
        });

        projectDetailsTemplate.on('upload', function (event) {
            var files = this.get('files');
            if (files.length > 0){
                new ProjectsApi().upload(this.get('project._id'), files).then(function(data) {
                    // TODO select first segment if list was empty
                    $.each(data.message, function (idx, segment) {
                        projectDetailsTemplate.push('project.segments', create_segment(segment));
                    });
                });
            }
        });

        projectDetailsTemplate.on('duplicate', function (event) {
            var segments = this.get('project.segments');

            var segments_with_duplicates = $.map(segments, function (segment, idx) {
                var s = [];
                s.push(segment);
                if (segment.checked) {
                    s.push(create_segment(segment));
                }
                segment.checked = false;
                return s;
            });

            this.set('project.segments', segments_with_duplicates);
        });

        projectDetailsTemplate.on('delete', function (event) {
            var active_segment = this.get('active_segment');
            var segments = this.get('project.segments');

            var segments_with_deletes = $.grep(segments, function (segment) {
                return segment.checked == false;
            });

            if ($.inArray(active_segment, segments_with_deletes) == -1) {
                active_segment = null;
            }

            this.set('project.segments', segments_with_deletes);
            this.set('active_segment', active_segment);
        });

        projectDetailsTemplate.on('next', function (event) {
            if (!isValid("#segment-details")) {
                return;
            }

            this.push_comment('active_segment.reviewer_comment', 'reviewer');
            this.push_comment('active_segment.translator_comment', 'translator');

            // empty event.keypath indicates that 'confirm and move forward' button was clicked
            var keypath = event.keypath;
            if (event.keypath == "") {
                this.confirm();
                keypath = 'project.segments.' + this.get_next_segment_index();
            }

            this.set({
                'active_segment': this.get(keypath),
                'project.segments': this.get('project.segments'),
            });
        });

        projectDetailsTemplate.on('agree', function (event) {
            this.set({
                'active_segment.error_disagreement': null,
                'active_segment.translator_comment': null,
            });

            this.fire('next', event);
        });

        projectDetailsTemplate.on('disagree', function (event) {
            this.set('active_segment.error_disagreement', 'correction');
        });

        projectDetailsTemplate.on('edit', function (event) {
            page('/projects/' + this.get('project._id') + '/edit');
        });

        projectDetailsTemplate.on('save', function (event) {
            if (!isValid("#segment-details")) {
                return;
            }

            var project = this.get('project');

            new ProjectsApi().save(project, function (data) {
                projectDetailsTemplate.set('project.lqi_result', data.message.lqi_result);
                swal("Good job!", "Project has been saved!", "success");
            });

        });

        projectDetailsTemplate.on('submit', function (event) {
            submit_project(projectDetailsTemplate, 'project');
        });

        projectDetailsTemplate.observe('project.phase', function (new_value, old_value, keypath) {
            var phase = new_value != null ? new_value : old_value;

            var context = this.get('context');
            var update_policy = this.get('update_policy');

            var policy = update_policy[phase][context];
            if (typeof policy === 'undefined') {
                var visibility_by_context = {
                    'manager': {
                        'project.reviewer_comment.visible': true,
                        'project.translator_comment.visible': true,
                    },
                    'reviewer': {
                        'project.reviewer_comment.visible': true,
                    },
                    'translator': {
                        'project.reviewer_comment.visible': true,
                        'project.translator_comment.visible': true,
                    }
                };

                policy = {
                    'project.enabled': false,
                    'project.evaluation.enabled': false,
                    'project.save.enabled': false,
                    'project.submission.enabled': false,
                    'project.reviewer_comment.enabled': false,
                    'project.reviewer_comment.visible': false,
                    'project.translator_comment.enabled': false,
                    'project.translator_comment.visible': false,
                    'project.dispute_resolution.visible': false,
                }

                policy = $.extend(policy, visibility_by_context[context])
            }
            this.set(policy);
        });

        setDataCheckAll();

    });

}


var CreateUpdateUserView = function(context) {
    CreateOrUpdateView.call(this, context, {
        resource_name: 'User',
        template: 'partial-user-form',
    });
};

$.extend(CreateUpdateUserView.prototype, CreateOrUpdateView.prototype, {

    fetch_resource: function (self) {
        if (this.edition) {
            return new UsersApi().get_by_id(this.context.params.id, function (data) {
                self.resource = data.message;
                self.resource.email_confirmation = self.resource.email;
            });
        }
        return self.resolved_deferred();
    },

    edition_enabled: function () {
        var is_manager = store.get('role') == 'manager';
        if (this.edition) {
            return is_manager || store.get('user_id') == this.user._id;
        }
        return is_manager;
    },

    save: function (resource, edition) {
        new UsersApi().save(resource, function (data) {
            page('/users');
        }, edition);
    },

});

function users() {
    new ListView({
        resource_name: 'User',
        api: new UsersApi(),
        data: {
            partial: 'partial-users-list',
            endpoint: '/users',
            filter: function (item) {
                var query = this.get('query');
                if (exists(query)) {
                    return item.username.match(query) || item.email.match(query);
                }
                return true;
            }
        }
    }).run();
}

function user_create_or_update(context) {
    new CreateUpdateUserView(context).run();
}


page('/', projects);
page('/projects', projects);
page('/projects/new', project_create_or_update);
page('/projects/:id', project_details);
page('/projects/:id/edit', project_create_or_update);
page('/users', users);
page('/users/new', user_create_or_update);
page('/users/:id/edit', user_create_or_update);
page();
