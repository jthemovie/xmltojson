# -*- coding: utf-8 -*-

from flask import request, render_template, redirect, make_response, Response
import xml.etree.ElementTree as ET
import urllib, json
from app import app


@app.route('/', methods=['GET'])
def index():
    return render_template('dashboard.html')


